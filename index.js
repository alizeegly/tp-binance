const express = require('express')
const app = express()

app.use(express.json())
const PORT = process.env.PORT || 8801

app.listen(PORT, () => {
    console.log(`Backend server is listening on port ${PORT} !`)
})